<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo get_page_link_current(); ?>" />
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<script type="text/javascript">
    var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
    // var path_dist = '<?php echo get_template_directory_uri(); ?>/dist/';
</script>

<div class="main-wrapper">

<header class="vk-header">

    <div class="vk-header__mid">
        <div class="container">
            <div class="vk-header__mid-content">
                <!-- logo -->
                <?php get_template_part("resources/views/logo"); ?>

                <div class="vk-header__mid-right">
                    <div class="_left">
                        <!-- search -->
                        <?php get_template_part("resources/views/search-form"); ?>

                    </div>

                    <!-- account -->
                    <div class="_right">
                        <ul class="vk-header__list">
                            <li><a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">Tài khoản</a></li>
                            <li><a href="<?php echo wp_login_url(); ?>">Đăng nhập</a></li>
                            <li><a href="<?php echo site_url('/wp-login.php?action=register'); ?>">Đăng ký</a></li>
                        </ul>

                        <div>
                            <!-- cart -->
                            <?php get_template_part("resources/views/wc/wc-info-cart"); ?>

                            <a href="#menu" data-menu="#menu" data class="vk-btn vk-btn--secondary vk-header__btn vk-header__btn--menu"><i class="ti-menu"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="vk-header__bot">
        <div class="container">
            <div class="vk-header__bot-content">
                <nav class="vk-header__menu">
                    <!-- menu -->
                    <?php get_template_part("resources/views/menu"); ?>
                    
                </nav>
            </div>
        </div>
    </div>

</header>

