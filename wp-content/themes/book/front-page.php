<?php get_header(); ?>

<h1 style="display: none;"><?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?></h1>

<?php
	//field
	$home_slide = get_field('home_slide');

	$home_product_select_post 	= get_field('home_product_select_post');

	$home_image_ads = get_field('home_image_ads');

	$home_product_new_title_section 	= get_field('home_product_new_title_section');
	$home_product_new_select_post 		= get_field('home_product_new_select_post');
	$home_product_new_url 				= get_field('home_product_new_url');

	$home_news_title_section 	= get_field('home_news_title_section');
	$home_news_select_post 	  	= get_field('home_news_select_post');
?>

<section class="vk-content">

<?php if(!empty( $home_slide )) { ?>
<div class="vk-home__banner pb-10">
    <div class="vk-banner__slider slick-slider" data-slider="banner">

		<?php
		    foreach ($home_slide as $foreach_kq) {

		    $post_image = $foreach_kq["image"];
		?>
	        <div class="_item">
	            <div class="vk-img"><img src="<?php echo $post_image; ?>" alt=""></div>
	        </div>
		<?php } ?>

    </div>
</div>
<?php } ?>

<?php if(!empty( $home_product_select_post )) { ?>
<div class="vk-home__shop">
    <div class="container">
        <div class="vk-shop__list slick-slider vk-slider--style-1" data-slider="shop">
            
			<?php
			    foreach ($home_product_select_post as $foreach_kq) {

				$post_id 			= $foreach_kq->ID;
				$post_title 		= get_the_title($post_id);
				// $post_content 		= wpautop(get_the_content($post_id));
				$post_date 			= get_the_date('d/m/Y',$post_id);
				$post_link 			= get_permalink($post_id);
				$post_image 		= getPostImage($post_id,"p-product");
				$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
				$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
				$post_tag 			= get_the_tags($post_id);

				$product = new WC_product($post_id);
			?>

				<div class="col-6 col-sm-6 col-md-4 col-lg-4 _item">
				    <div class="vk-shop-item vk-shop-item--style-1 woocommerce">
				        <div class="vk-shop-item__img">
				            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
				            <a href="<?php echo $post_link; ?>" class="vk-shop-item__link"></a>
				            <div class="vk-shop-item__button">
				                <div class="vk-shop-item__btn">
				                    <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
				                </div>
				                <a href="javascript:void(0)" class="vk-shop-item__btn">
				                    <?php echo show_add_to_cart_button($post_id); ?>
				                </a>
				                <a href="javascript:void(0)" class="vk-shop-item__btn hover-product" data-productid="<?php echo $post_id; ?>">
				                	<img src="<?php echo asset('images/icon-6.png'); ?>" alt="">
				                </a>
				            </div>
				        </div>
				        <div class="vk-shop-item__brief">
				            <h3 class="vk-shop-item__title">
				            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
				            		<?php echo $post_title; ?>
				            	</a>
				            </h3>
				            <div class="vk-shop-item__rate">
				            	<?php echo show_rating($post_id); ?>
				            </div>
				        </div>
				    </div>
				</div>

			<?php } ?>

        </div>
    </div>
</div>
<?php } ?>

<?php if(!empty( $home_image_ads )) { ?>
<div class="container pt-20 pb-30">
    <div class="text-center">
        <img src="<?php echo $home_image_ads; ?>" alt="">
    </div>
</div>
<?php } ?>

<div class="vk-home__shop pb-20">
    <div class="container">
        <h2 class="vk-home__title"><?php echo $home_product_new_title_section; ?></h2>

        <?php if(!empty( $home_product_new_select_post )) { ?>
        <div class="vk-shop__list row">

			<?php
			    foreach ($home_product_new_select_post as $foreach_kq) {

				$post_id 			= $foreach_kq->ID;
				$post_title 		= get_the_title($post_id);
				// $post_content 		= wpautop(get_the_content($post_id));
				$post_date 			= get_the_date('d/m/Y',$post_id);
				$post_link 			= get_permalink($post_id);
				$post_image 		= getPostImage($post_id,"p-product");
				$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
				$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
				$post_tag 			= get_the_tags($post_id);

				$product = new WC_product($post_id);
			?>

				<div class="col-6 col-sm-6 col-md-4 col-lg-3 _item">
				    <div class="vk-shop-item vk-shop-item--style-1 woocommerce">
				        <div class="vk-shop-item__img">
				            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
				            <a href="<?php echo $post_link; ?>" class="vk-shop-item__link"></a>
				            <div class="vk-shop-item__button">
				                <div class="vk-shop-item__btn">
				                    <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
				                </div>
				                <a href="javascript:void(0)" class="vk-shop-item__btn">
				                    <?php echo show_add_to_cart_button($post_id); ?>
				                </a>
				                <a href="javascript:void(0)" class="vk-shop-item__btn hover-product" data-productid="<?php echo $post_id; ?>">
				                	<img src="<?php echo asset('images/icon-6.png'); ?>" alt="">
				                </a>
				            </div>
				        </div>
				        <div class="vk-shop-item__brief">
				            <h3 class="vk-shop-item__title">
				            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
				            		<?php echo $post_title; ?>
				            	</a>
				            </h3>
				            <div class="vk-shop-item__rate">
				            	<?php echo show_rating($post_id); ?>
				            </div>
				        </div>
				    </div>
				</div>

			<?php } ?>

        </div>
        <?php } ?>

    </div>
</div>

<?php if(!empty( $home_product_new_url )) { ?>
<div class="vk-shop__button">
    <div class="container">
        <a href="<?php echo $home_product_new_url; ?>" class="vk-shop-detail__btn">Xem thêm</a>
    </div>
</div>
<?php } ?>

<div class="vk-home__blog pt-30 pb-50">
    <div class="container">
        <h2 class="vk-home__title"><?php echo $home_news_title_section; ?></h2>

        <?php if(!empty( $home_news_select_post )) { ?>
        <div class="vk-blog__list row">

			<?php
			    foreach ($home_news_select_post as $foreach_kq) {

			    $post_id 			= $foreach_kq->ID;
				$post_title 		= get_the_title($post_id);
				// $post_content 		= wpautop(get_the_content($post_id));
				$post_date 			= 'Ngày '.get_the_date('d.m.Y',$post_id);
				$post_link 			= get_permalink($post_id);
				$post_image 		= getPostImage($post_id,"p-post");
				$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
				$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
				$post_tag 			= get_the_tags($post_id);
			?>

				<div class="col-sm-6 col-md-3 col-lg-4 _item">
				    <div class="vk-blog-item vk-blog-item--style-1">
				        <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-blog-item__img">
				            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
				        </a>
				        <div class="vk-blog-item__brief">
				            <h3 class="vk-blog-item__title">
				            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
					            	<?php echo $post_title; ?>
					            </a>
				        	</h3>
				            <div class="vk-blog-item__date">
					            <?php echo $post_date; ?>
					        </div>
				            <div class="vk-blog-item__text" data-truncate-lines="3">
					            <?php echo $post_excerpt; ?>
					        </div>
				        </div>
				    </div>
				</div>

			<?php } ?>

        </div>
        <?php } ?>

    </div>
</div>

</section>

<?php get_footer(); ?>

