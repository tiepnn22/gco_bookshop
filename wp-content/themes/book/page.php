<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content(); //woo phải dùng the_content()

	//banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="vk-about__content">
        <div class="container">
            <div class="vk-about__box <?php if( is_cart() || is_checkout() ) {} else { echo 'wp-editor-fix'; } ?>">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    
</section>

<?php get_footer(); ?>