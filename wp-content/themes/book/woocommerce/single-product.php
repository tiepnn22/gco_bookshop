<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php
	$product_id = get_the_ID();

	$terms = wp_get_object_terms($post->ID, 'product_cat');
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
	$term_id 	= $term->term_id;
	$term_name  = $term->name;

    // woocommerce
    $product = new WC_product($product_id);

    // wc tag
	// $single_product_tag = $product->get_tags();

    // wc gallery
    // $single_product_gallery = $product->get_gallery_image_ids();

	// wc money
	// $money =  wc_get_product($product_id);
	// $old_price = (float)$money->get_regular_price();
	// $price = (float)$money->get_sale_price();

	// wc stock
    // if ( ! $product->managing_stock() && ! $product->is_in_stock() ) {
	//     $product_stock = 'Hết hàng';
	// } else {
	// 	$product_stock = 'Còn hàng';
	// }

	// wc sku
    $product_sku = $product->get_sku();

    // info product
    $single_product_title   = get_the_title($product_id);
    $single_product_content = wpautop(get_the_content($product_id));
    $single_product_date    = get_the_date('d/m/Y', $product_id);
    $single_product_link    = get_permalink($product_id);
    $single_product_image   = getPostImage($product_id,"full");
    $single_product_excerpt = wpautop(get_the_excerpt($product_id));
    $single_recent_author   = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
    $single_product_author  = $single_recent_author->display_name;

	//banner
    $data_page_banner  = array(
        'image_alt'    =>    $single_product_title
    );
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="vk-shop__box">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="vk-shop-detail__top">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="vk-shop-detail__thumbnail">
                                    <div class="vk-shop-detail__for">
                                        <a class="MagicZoom" id="zoom" data-options="textHoverZoomHint: Di qua để phóng to; textExpandHint: Click để xem chi tiết" title="..." href="<?php echo $single_product_image; ?>">
                                            <img src="<?php echo $single_product_image; ?>" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 pt-lg-0 pt-30">
                                <div class="vk-shop-detail__brief">
                                    <div class="vk-shop-detail__label"><?php echo $term_name; ?></div>
                                    <h1 class="vk-shop-detail__heading"><?php echo $single_product_title; ?></h1>
                                    <div class="vk-shop-detail__rate">
                                        <?php echo show_rating($product_id); ?>
                                    </div>
                                    <ul class="vk-shop-detail__list">
                                        <li> Mã sách: <?php echo $product_sku; ?></li>
                                        <li> Giá bán: <?php echo show_price_old_price($product_id); ?></li>
                                    </ul>
                                    <div class="vk-shop-detail__short-content">
                                        <p><?php echo $single_product_excerpt; ?></p>
                                    </div>
                                    <?php echo show_attributes($product_id); ?>
                                    <div class="vk-shop-detail__qty">
                                        <?php echo show_add_to_cart_button_quantity($product_id); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vk-shop-detail__bot">
                        <h2 class="vk-shop-detail__title"><span>Thông tin sách</span></h2>
                        <div class="vk-shop-detail__content wp-editor-fix">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <div class="vk-shop-detail__bot">
                        <h2 class="vk-shop-detail__title"><span>Bình luận</span></h2>
                        <div class="vk-shop-detail__content">
                            <?php
                                if ( comments_open() || get_comments_number() ) {
                                    comments_template();
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="vk-shop__button">
        <div class="container">
            <a href="shop.html" class="vk-shop-detail__btn">Xem thêm</a>
        </div>
    </div> -->

    <?php get_template_part("resources/views/template-related-product"); ?>

</section>

<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
