<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

    $term_info      = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_id        = $term_info->term_id;
    $term_name      = $term_info->name;
    $term_desc      = cut_string( $term_info->description ,300,'...');
    $term_link      = esc_url(get_term_link($term_id));
    $taxonomy_slug  = $term_info->taxonomy;
    $thumbnail_id       = get_woocommerce_term_meta( $term_id, 'thumbnail_id', true );
    $term_image_check   = wp_get_attachment_url( $thumbnail_id );
    $term_image         = (!empty($term_image_check)) ? $term_image_check : asset('images/banner-1.jpg');
    // $term_childs    = get_term_children( $term_id, $taxonomy_slug );
    // $count          = count($term_childs);

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $term_name
    );
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="container pb-50">
        <div class="row">

            <?php do_action( 'woocommerce_sidebar' ); ?>

            <div class="col-lg-9 order-0 order-lg-1">
                <div class="vk-banner vk-banner--shop">
                    <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
                </div>
                <p><?php echo $term_desc; ?></p>

                <?php
                    if ( woocommerce_product_loop() ) {
                        // do_action( 'woocommerce_before_shop_loop' );
                ?>

                        <div class="vk-shop__list row">
                        <?php
                            // woocommerce_product_loop_start();
                            if ( wc_get_loop_prop( 'total' ) ) {
                                while ( have_posts() ) {
                                    the_post();
                                    // do_action( 'woocommerce_shop_loop' );
                                    
                                    // wc_get_template_part( 'content', 'product' );
                                    get_template_part('resources/views/content/category-product', get_post_format());
                                }
                            }
                            // woocommerce_product_loop_end();
                        ?>
                        </div>

                <?php
                        do_action( 'woocommerce_after_shop_loop' );
                    } else {
                        do_action( 'woocommerce_no_products_found' );
                    }
                ?>
            </div>

        </div>
    </div>
</section>


<?php
get_footer( 'shop' );
?>