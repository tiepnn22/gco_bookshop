<?php
	/*
	Template Name: Mẫu Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );

    //field
    $contact_contact_form_id    = get_field('contact_contact_form');
    $contact_contact_form       = do_shortcode('[contact-form-7 id="'.$contact_contact_form_id.'"]');
?>

<section class="vk-content">

    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="container pb-50 pt-20">
        <div class="vk-contact__content">

            <?php if(!empty( $contact_contact_form )) { ?>
            <div class="vk-form--contact">
                <?php echo $contact_contact_form; ?>
            </div>
            <?php } ?>

        </div>
    </div>
    
</section>

<?php get_footer(); ?>