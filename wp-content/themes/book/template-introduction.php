<?php
	/*
	Template Name: Mẫu Giới thiệu
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );

    //field
    $intro_image_ads      = get_field('intro_image_ads');

    $intro_value_title    = get_field('intro_value_title');
    $intro_value_desc     = get_field('intro_value_desc');
    $intro_value_image    = get_field('intro_value_image');
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="vk-about__content">
        <div class="container">
            <div class="vk-about__box">
                <?php echo $page_content; ?>
            </div>
        </div>

        <div class="vk-img mb-30">
            <img src="<?php echo $intro_image_ads; ?>" alt="">
        </div>

        <div class="container pb-50">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-lg-7">
                            <h5><?php echo $intro_value_title; ?></h5>
                            <p><?php echo $intro_value_desc; ?></p>
                        </div>
                        <div class="col-lg-5">
                            <img src="<?php echo $intro_value_image; ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>

<?php get_footer(); ?>