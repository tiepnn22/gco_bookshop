<?php
include_once get_template_directory(). '/load/Custom_Functions.php';
// include_once get_template_directory(). '/load/CTPost_CTTax.php';
include_once get_template_directory(). '/load/Performance.php';
include_once get_template_directory(). '/load/wc.php';


/* Create CTPost */
// (title, slug_code, slug)
// create_post_type("Sản phẩm","product","sanpham");
/* Create CTTax */
// (title, slug, slug_code, post_type)
// create_taxonomy_theme("Danh mục Sản phẩm","danhmuc-sanpham","product_cat","product");


// Create menu Theme option use Acf Pro
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Tuỳ chỉnh',      // Title hiển thị khi truy cập vào Options page
        'menu_title'    => 'Tuỳ chỉnh',      // Tên menu hiển thị ở khu vực admin
        'menu_slug'     => 'theme-settings', // Url hiển thị trên đường dẫn của options page
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}


// Title Head Page
if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_option('blogname') .' - '. get_option('blogdescription');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            $return = $obj->name;
                if($return == 'product'){
                    $return_kq = __( 'Sản phẩm', 'text_domain' );
                }else{
                    $return_kq = $return;
                }
            return $return_kq;
        }

        if (is_search()) {
            return __( 'Tìm kiếm cho', 'text_domain' ).' : ['.$_GET['s'].']';
        }

        if (is_404()) {
            return __( '404 Không tìm thấy trang', 'text_domain' );
        }

        return get_the_title();
    }
}


// Url File theme
if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}


// Url image theme
if (!function_exists('getPostImage')) {
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset('images/no-image-wc.png') : $img[0];
    }
}


// Cut String text
if (!function_exists('cut_string')) {
    function cut_string($str,$len,$more){
        if ($str=="" || $str==NULL) return $str;
        if (is_array($str)) return $str;
            $str = trim(strip_tags($str));
        if (strlen($str) <= $len) return $str;
            $str = substr($str,0,$len);
        if ($str != "") {
            if (!substr_count($str," ")) {
              if ($more) $str .= " ...";
              return $str;
            }
            while(strlen($str) && ($str[strlen($str)-1] != " ")) {
                $str = substr($str,0,-1);
            }
            $str = substr($str,0,-1);
            if ($more) $str .= " ...";
        }
        return $str;
    }
}


// Count Views Post
if (!function_exists('getPostViews')) {
    function getPostViews($postID){
        $count_key = 'post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
            return "0";
        }
        return $count;
    }
}
if (!function_exists('setPostViews')) {
    function setPostViews($postID) {
        $count_key = 'post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            $count = 0;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
        }else{
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }
}


// Get url page here
if (!function_exists('get_page_link_current')) {
    function get_page_link_current(){
        // Get url page here
        $page_link_current_get = sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['REQUEST_URI']
        );

        // Get url, exclude url GET
        $page_link_current_strstr = strstr($page_link_current_get, '?');
        $page_link_current        = str_replace( $page_link_current_strstr, '', $page_link_current_get );

        return $page_link_current;
    }
}


// Get url page template by name file (vd : template-contact.php)
if (!function_exists('get_link_page_template')) {
    function get_link_page_template($name_file){
        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => $name_file
        ));
        
        $page_template_link = get_page_link($pages[0]->ID);

        return $page_template_link;
    }
}


// Pagination
function paginationCustom($max_num_pages) {
    echo '<ul class="vk-list vk-list--inline vk-pagination__list">';
    if ($max_num_pages > 1) {   // tổng số trang (10)
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; // trang hiện tại (8)

        if ($max_num_pages > 9) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( 1 ) ).'" class="item">
            ...</a></li>';
        }
        if ($paged > 1) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $paged - 1 ) ).'" class="item">
            «</i></a></li>';
        }
        if ($paged >= 5 && $max_num_pages > 9) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $paged - 5 ) ).'" class="item">
            -5</a></li>';
            echo '<li class="vk-list__item"><a href="javascript:void(0)" class="">&nbsp;</a></li>';
        }

        for($i= 1; $i <= $max_num_pages; $i++) {
            // $half_total_links = floor( 5 / 2);
            $half_total_links = 2;

            $from = $paged - $half_total_links; // trang hiện tại - 2 (8-2= 6)
            $to = $paged + $half_total_links;   // trang hiện tại + 2 (8+2 = 10)

            if ($from < $i && $i < $to) {   // $form cách $to 3 số (từ 6 đến 10 là 7,8,9)
                $class = $i == $paged ? 'active' : 'item';
                echo '<li class="vk-list__item '.$class.'"><a href="'.esc_url( get_pagenum_link( $i ) ).'" class="">'.$i.'</a></li>';
            }
        }

        if ($paged <= $max_num_pages - 5 && $max_num_pages > 9) {
            echo '<li class="vk-list__item"><a href="javascript:void(0)" class="">&nbsp;</a></li>';
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $paged + 5 ) ).'" class="item">
            +5</a></li>';
        }
        if ($paged + 1 <= $max_num_pages) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $paged + 1 ) ).'" class="item">
            »</i></a></li>';
        }
        if ($max_num_pages > 9) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $max_num_pages ) ).'" class="item">
            ...</a></li>';
        }
    }
    echo '</ul>';
}


// Ajax Readmore_post Product
add_action('wp_ajax_Readmore_post', 'Readmore_post');
add_action('wp_ajax_nopriv_Readmore_post', 'Readmore_post');
function Readmore_post() {
    global $product;

    $data_productid = $_POST['data_productid'];
    $data = array();

    $post_id            = $data_productid;
    $post_title         = get_the_title($post_id);
    $post_link          = get_permalink($post_id);
    $post_image         = getPostImage($post_id,"p-product");
    $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');

    // terms
    $terms = wp_get_object_terms($post_id, 'product_cat');
    if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
    $term_id = $term->term_id;
    $term_name = $term->name;

    // wc
    $product = wc_get_product( $post_id );
    $product_sku = $product->sku;

    // $data['exit'] = $data_productid;

    $data['result'] .= '
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-body">
            <div class="vk-shop-detail__top woocommerce">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="vk-shop-detail__thumbnail">
                            <div class="vk-shop-detail__for">
                                <a class="MagicZoom" id="zoom" data-options="textHoverZoomHint: Di qua để phóng to; textExpandHint: Click để xem chi tiết" title="..." href="'.$post_image.'">
                                    <img src="'.$post_image.'" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pt-lg-0 pt-30">
                        <div class="vk-shop-detail__brief">
                            <div class="vk-shop-detail__label">'.$term_name.'</div>
                            <h1 class="vk-shop-detail__heading">'.$post_title.'</h1>
                            <div class="vk-shop-detail__rate">'.show_rating($post_id).'</div>
                            <ul class="vk-shop-detail__list">
                                <li> Mã sách: '.$product_sku.'</li>
                                <li> Giá bán: '.show_price_old_price($post_id).'</li>
                            </ul>
                            <div class="vk-shop-detail__short-content">
                                <p>
                                    '.$post_excerpt.'
                                </p>
                            </div>
                            '.show_attributes($post_id).'
                            <div class="vk-shop-detail__qty">
                                '.show_add_to_cart_button_quantity($post_id).'
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/dist/plugin/magiczoomplus/magiczoomplus.min.css">
        <script type="text/javascript" src="'.get_stylesheet_directory_uri().'/dist/plugin/magiczoomplus/magiczoomplus.js"></script>
    ';

    echo json_encode($data);
    die();
}