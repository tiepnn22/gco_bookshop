<?php
    //field
    $f_subscribe_title    = get_field('f_subscribe_title', 'option');
    $f_subscribe_form_id  = get_field('f_subscribe_form', 'option');
    $f_subscribe_form     = do_shortcode('[contact-form-7 id="'.$f_subscribe_form_id.'"]');

    $f_socical_facebook   = get_field('f_socical_facebook', 'option');
    $f_socical_twiter     = get_field('f_socical_twiter', 'option');
    $f_socical_linkin     = get_field('f_socical_linkin', 'option');
    $f_socical_skype      = get_field('f_socical_skype', 'option');

    $f_all_place          = get_field('f_all_place', 'option');

    $f_bottom_copyright   = get_field('f_bottom_copyright', 'option');
?>

<div class="vk-newsregister">
    <div class="container">
        <div class="vk-newsregister__content">
            <div class="vk-form--register">
                <div class="_label"><?php echo $f_subscribe_title; ?></div>
                
                <?php if(!empty( $f_subscribe_form )) { ?>
                    <?php echo $f_subscribe_form; ?>
                <?php } ?>

            </div>
        </div>
    </div>
</div>

<footer class="vk-footer">
    <div class="vk-footer__top">
        <div class="container">

            <div class="vk-footer__before">
                <div class="vk-footer__before-content">
                    <div class="_left">
                        <?php get_template_part("resources/views/logo-footer"); ?>
                    </div>
                    <div class="_right">
                        <ul class="vk-footer__list vk-footer__list--style-1">
                            <li>
                                <a title href="<?php echo $f_socical_facebook; ?>" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a title href="<?php echo $f_socical_twiter; ?>" target="_blank">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a title href="<?php echo $f_socical_linkin; ?>" target="_blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a title href="<?php echo $f_socical_skype; ?>" target="_blank">
                                    <i class="fa fa-skype"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php if(!empty( $f_all_place )) { ?>
            <div class="vk-footer__top-content row">

                <?php
                    foreach ($f_all_place as $foreach_kq) {

                    $post_title   = $foreach_kq["title"];
                    $post_address = $foreach_kq["address"];
                    $post_phone   = $foreach_kq["phone"];
                ?>
                    <div class="col-lg-4">
                        <div class="vk-footer__item">
                            <div class="vk-footer__text">
                                <h3><?php echo $post_title; ?></h3>
                                <ul class="vk-footer__list vk-footer__list--style-2">
                                    <li>
                                        <i class="_icon fa fa-map-marker"></i> <?php echo $post_address; ?>
                                    </li>
                                    <li>
                                        <i class="_icon fa fa-phone"></i> <b>Điện thoại:</b> 
                                        <a href="tel:<?php echo str_replace(' ','',$post_phone);?>"><?php echo $post_phone; ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
            <?php } ?>
            
        </div>
    </div>

    <div class="vk-footer__bot">
        <div class="container">
            <div class="vk-footer__bot-content">
                <?php echo $f_bottom_copyright; ?>
            </div>
        </div>
    </div>
</footer>

<!--menu mobile-->
<nav class="d-lg-none" id="menu">
    <?php get_template_part("resources/views/menu"); ?>
</nav>

<!-- quickview -->
<div class="modal qv-modal" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="ajaxLoad"></div>

<?php get_template_part("resources/views/socical-footer"); ?>

<?php wp_footer(); ?>
</div>

</body>
</html>