<?php get_header(); ?>

<?php
	$s = $_GET['s'];
	$cate = $_GET['cate'];

	$page_name = __('Tìm kiếm', 'text_domain');
	$cat_id = (!empty($cate)) ? $cate : '';

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="container pb-50">
        <div class="row">
            <div class="col-lg-12 order-0 order-lg-1">
            	<div class="vk-shop__list row woocommerce">

		            <?php
		            	if( !empty($cat_id) ) {
		                	$query = query_search_post_by_taxonomy_paged($s, $cat_id, 'product', 'product_cat', 3);
		            	} else {
		            		$query = query_search_post_paged($s, array('product'), 3);
		            	}
		                $max_num_pages = $query->max_num_pages;

		                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
		            ?>

						<?php get_template_part('resources/views/content/category-product', get_post_format()); ?>

		            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

	        	</div>
            </div>
        </div>
    </div>

    <!--pagination-->
    <nav class="vk-pagination">
        <?php echo paginationCustom( $max_num_pages ); ?>
    </nav>

</section>

<?php get_footer(); ?>