<?php
    $terms = get_terms('product_cat', array(
        'parent'=> 0,
        'hide_empty' => false
    ) );
?>

<div class="vk-header__search">
    <!-- <div class="vk-form vk-form--search"> -->

    	<form class="vk-form vk-form--search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	        <select name="cate" id="" class="form-control">
	            <option value="">Danh mục sách</option>
				
				<?php
				    foreach ($terms as $foreach_kq) {
					    $post_id = $foreach_kq->term_id;
					    $post_title = $foreach_kq->name;
			    ?>
			    	<option <?php if($_GET['cate'] == $post_id) { echo 'selected'; }?> value="<?php echo $post_id; ?>">
			    		<?php echo $post_title; ?>
		    		</option>
			    <?php
					}
				?>

	        </select>
	        <input type="text" class="form-control" required="required" placeholder="<?php _e('Tìm kiếm...', 'text_domain'); ?>" name="s" value="<?php echo get_search_query(); ?>">
	        <button type="submit" class="vk-btn"><i class="ti-search mr-10"></i></button>
	    </form>

    <!-- </div> -->
</div>