<?php
	global $data_page_banner;
	
	if(!empty( $data_page_banner )) {
		$image_alt 	= $data_page_banner['image_alt'];
	}
?>

<div class="vk-breadcrumb">
    <nav class="container">
        <div class="vk-breadcrumb__content">
            <div class="_box">

                <h1 class="_title"><?php echo $image_alt;?></h1>

                <div class="vk-breadcrumb__list">
			        <?php
			            if(function_exists('bcn_display')) { 
			                echo '<a href="' . site_url() . '">Trang chủ </a> /';
			                bcn_display(); 
			            }
			        ?>
			    </div>

            </div>
        </div>
    </nav>
</div>