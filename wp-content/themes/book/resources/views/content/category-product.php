<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	$post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-product");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<div class="col-6 col-sm-6 col-md-4 col-lg-4 _item">
    <div class="vk-shop-item vk-shop-item--style-1">
        <div class="vk-shop-item__img">
            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
            <a href="<?php echo $post_link; ?>" class="vk-shop-item__link"></a>
            <div class="vk-shop-item__button">
                <div class="vk-shop-item__btn">
                    <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
                </div>
                <a href="javascript:void(0)" class="vk-shop-item__btn">
                    <?php echo show_add_to_cart_button($post_id); ?>
                </a>
                <a href="javascript:void(0)" class="vk-shop-item__btn hover-product" data-productid="<?php echo $post_id; ?>">
                	<img src="<?php echo asset('images/icon-6.png'); ?>" alt="">
                </a>
            </div>
        </div>
        <div class="vk-shop-item__brief">
            <h3 class="vk-shop-item__title">
            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
            		<?php echo $post_title; ?>
            	</a>
            </h3>
            <div class="vk-shop-item__rate">
            	<?php echo show_rating($post_id); ?>
            </div>
        </div>
    </div>
</div>