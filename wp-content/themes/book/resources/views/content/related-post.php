<?php
    $post_id            = get_the_ID();
    $post_title         = get_the_title($post_id);
    $post_content       = wpautop(get_the_content($post_id));
    $post_date          = 'Ngày '.get_the_date('d.m.Y',$post_id);
    $post_link          = get_permalink($post_id);
    $post_image         = getPostImage($post_id,"p-post");
    $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');
    $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
    $post_tag           = get_the_tags($post_id);
    $post_comment       = wp_count_comments($post_id);
    $post_comment_total = $post_comment->total_comments;
?>

<div class="col-sm-6 col-md-6 _item">
    <div class="vk-blog-item vk-blog-item--style-1">
        <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-blog-item__img">
            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
        </a>
        <div class="vk-blog-item__brief">
            <h3 class="vk-blog-item__title">
                <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                    <?php echo $post_title; ?>
                </a>
            </h3>
            <div class="vk-blog-item__date">
                <?php echo $post_date; ?>
            </div>
            <div class="vk-blog-item__text" data-truncate-lines="3">
                <?php echo $post_excerpt; ?>
            </div>
        </div>
    </div>
</div>