<?php
    if(function_exists('wp_nav_menu')){
        $args = array(
            'theme_location' 	=> 	'primary',
            'container_class'	=>	'menu-chinh-container',
            'menu_class'		=>	'vk-menu__main'
        );
        wp_nav_menu( $args );
    }
?>