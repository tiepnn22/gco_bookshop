<?php
	global $post;
	$terms 		= get_the_terms( $post->ID , 'product_cat', 'string');
	$term_ids 	= wp_list_pluck($terms,'term_id');
	
	$query = new WP_Query( array(
		'post_type' 	 => 'product',
		'tax_query' 	 => array(
			array(
				'taxonomy' 	=> 'product_cat',
				'field' 	=> 'id',
				'terms' 	=> $term_ids,
				'operator'	=> 'IN'
			 )),
		'posts_per_page' => -1,
		'orderby' 		 => 'date',
		'post__not_in'	 => array($post->ID)
	) );
?>

<div class="vk-shop__box">
    <div class="container">
        <div class="vk-shop__relate">
            <h2 class="vk-shop__title">Sản phẩm liên quan</h2>
            <div class="row slick-slider vk-slider--style-1" data-slider="relate">

				<?php
					if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
				?>

					<?php get_template_part('resources/views/content/related-product', get_post_format()); ?>

				<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            </div>
        </div>
    </div>
</div>