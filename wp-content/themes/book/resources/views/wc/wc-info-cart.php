<?php
    //cart
    $info_cart     = WC()->cart->cart_contents;
    $cart_count    = WC()->cart->get_cart_contents_count();
    $cart_total    = WC()->cart->get_cart_total();
    $cart_page_url = wc_get_cart_url();

    //checkout
    $checkout_page_url = wc_get_checkout_url();
?>

<div class="vk-header__minicart">
    <a href="shopcart.html" class="vk-header__btn--minicart">
        <img src="<?php echo asset('images/icon-2.png'); ?>" alt="">
    </a>
    <div class="vk-minicart">

        <?php if($cart_count > 0) { ?>

            <div class="vk-minicart__list">
            
                <?php
                    foreach ($info_cart as $info_cart_kq) {
                        
                    $product_id         = $info_cart_kq["product_id"];
                    $product_title      = get_the_title($product_id);
                    $product_link       = get_permalink($product_id);
                    $product_image      = getPostImage($product_id,"p-product");
                    $post_excerpt       = cut_string(get_the_excerpt($product_id),300,'...');

                    // $money              =  wc_get_product($product_id);
                    // $old_price          = (float)$money->get_regular_price();
                    // $price              = (float)$money->get_sale_price();

                    $product_quantity   = $info_cart_kq["quantity"];
                ?>

                    <div class="vk-minicart-item">
                        <a href="<?php echo $product_link; ?>" class="vk-minicart-item__img">
                            <img src="<?php echo $product_image; ?>" alt="<?php echo $product_title; ?>">
                        </a>
                        <div class="vk-minicart-item__brief">
                            <h3 class="vk-minicart-item__title">
                                <?php echo $product_title; ?>
                            </h3>
                            <div class="vk-minicat-item__text">
                                <?php echo show_price_old_price($product_id); ?>
                            </div>
                        </div>
                        <div class="">
                            <a title href="javascript:void(0)">
                                x <?php echo $product_quantity; ?>
                            </a>
                        </div>
                    </div>

                <?php } ?>
                
            </div>

            <div class="vk-minicart__total">
                <span>Tổng:</span>
                <span><?php echo $cart_total; ?></span>
            </div>

            <div class="vk-minicart__button">
                <a href="<?php echo $cart_page_url; ?>" class="vk-minicart__btn">Giỏ hàng</a>
                <a href="<?php echo $checkout_page_url; ?>" class="vk-minicart__btn">Thanh toán</a>
            </div>

        <?php } else { ?>
            <?php _e('Giỏ hàng trống !', 'text_domain'); ?>
        <?php } ?>

    </div>
</div>