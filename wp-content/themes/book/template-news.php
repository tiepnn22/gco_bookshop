<?php
	/*
	Template Name: Mẫu Tin tức
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="container pb-20">
        <div class="vk-blog__list row">

            <?php
                $query = query_post_by_custompost_paged('post', 4);
                $max_num_pages = $query->max_num_pages;

                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>

                <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>
    </div>

    <!--pagination-->
    <nav class="vk-pagination">
        <?php echo paginationCustom( $max_num_pages ); ?>
    </nav>

</section>

<?php get_footer(); ?>